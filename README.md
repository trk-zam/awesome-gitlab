# Awesome GitLab

> Curated List of resources that are all about GitLab

Do you know other awesome GitLab resources, then please contribute !

## How to use GitLab

- [GitLab.com](https://gitlab.com) - GitLab Homepage
- [CE User Documentation](https://docs.gitlab.com/ce/README.html) - Documentation for the GitLab Community Edition
- [EE User Documentation](https://docs.gitlab.com/ee/README.html) - Documentation for the Gitlab Enterprise Edition

### Tools for GitLab

- [Create Snippets from the command line](https://gitlab.com/zj/snippet)

## Installing GitLab


## Development of GitLab

- [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit) - Start here for contributing to GitLab
- [GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce) - CE Version

## GitLab the Company

- [The Handbook](https://about.gitlab.com/handbook/) - The famous Handbook of the company
- [GitLab Blog](https://about.gitlab.com/blog/) - Stay updated on the different GitLab topics
- [GitLab on Twitter](https://twitter.com/gitlab) - The official Twitter Account
- [The Team](https://about.gitlab.com/team/) - The team behind GitLab
